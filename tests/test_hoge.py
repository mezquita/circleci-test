# -*- coding: utf-8 -*-
from mods.hoge import Hoge
import pytest

class TestHoge1(object):

    @pytest.fixture()
    def hoge(request):
        return Hoge(1)

    def test_type(self, hoge):
        assert isinstance(hoge, Hoge)

    def test_amari(self, hoge):
        assert hoge.amari(2)==0

    @pytest.mark.parametrize("n", [x for x in range(6) if x%2==0])
    def test_amari_sononi(self, hoge, n):
        # 0から5の数字を次々に代入してテスト
        assert hoge.amari(n)==0

    @pytest.mark.parametrize("n", [2,4])
    def test_amari_sonoyon(self, hoge, n):
        assert hoge.amari(n)==0




if __name__ == '__main__':
    pytest.main()